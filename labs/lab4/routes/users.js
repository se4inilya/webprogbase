const express = require('express');
const users = require('../models/user');
const router = express.Router();
//const usersJSON = require('../data/users');


router.get('/:id', (req, res) => {
  console.log('ID route called');
  users.getById(parseInt(req.params.id), (err, data) => {
    if (err) {
      res.status(404).send("Error 404");
    }
    else {
      res.render('user', {
        layout: 'layout',
        title: 'User ' + req.params.id,
        login: data.login,
        fullname: data.fullname,
        registeredAt: data.registeredAt,
        image: data.avaUrl
      });
    }
  });

});

router.get('/', (req, res) => {
  users.getAll((err, data) => {
    if (err)
      res.status(404).send("Error 404");
    else {
      console.log(data);
      res.render('users', {
        layout: 'layout',
        title: 'Users',
        users: data
      });
    }
  });

});


module.exports = router;