const express = require('express');
const bots = require("../models/bot");
const router = express.Router();
const fs = require("fs");
let multer = require("multer");



const upload = multer({
  dest: '/home/iluha/projects/webprogbase/labs/lab4/data/temp'
});




router.get('/', (req, res) => {
  bots.getAll((err, data) => {
    if (err)
      res.status(404).send("Error 404");
    else {
      let searchLine = req.query.search;
      let partOfBots = [];
      let page = parseInt(req.query.page);
      let prevN = page - 1;
      let nextN = page + 1;
      let botQuantity = 3;
      let pageAll = Math.ceil(data.length / botQuantity);
      let ErrorSearch;
      let Result;
      let prev_disabled = "";
      let next_disabled = "";
      if (searchLine) {
        Result = 'Result of search "' + searchLine + '" by owner names';
        for (let i = 0; i < data.length; i++) {
          if (data[i].owner.includes(searchLine))
            partOfBots.push(data[i]);

        }
        pageAll = Math.ceil(partOfBots.length / botQuantity);
        if (partOfBots.length == 0)
          ErrorSearch = "Bot with this owner does not exist";
        pageAll++;
      }
      else {
        // console.log(req.query.page);
        Result = "List of bots";
        page = parseInt(req.query.page);
        if (!page || page == 1) {
          page = 1;
          prevN = page;

          if (pageAll == 1) {
            nextN = page;
          }
          else
            nextN = page + 1;
        }
        else if (page == pageAll) {
          page = pageAll
          nextN = page;

        }
        partOfBots = data.slice(page * botQuantity - botQuantity, page * botQuantity);
      }
      if (!page || page == 1) {
        page = 1;
        prevN = page;
        prev_disabled = "btn disabled";
        if (pageAll == 1) {
          nextN = page;
          next_disabled = "btn disabled";
        }
      }
      else if (page == pageAll) {
        page = pageAll
        nextN = page;
        next_disabled = "btn disabled";
      }

      res.render('bots', {
        title: "Bots",
        layout: 'layout',
        bots: partOfBots,
        page: page,
        quanOfPage: pageAll,
        pagePrevNumber: prevN,
        pageNextNumber: nextN,
        ErrorSearch: ErrorSearch,
        Result: Result,
        prev_disabled: prev_disabled,
        next_disabled: next_disabled
      });
    }
  });
});



router.get('/:id(\\d+)', (req, res) => {
  console.log("id : " + req.params.id);
  bots.getById(parseInt(req.params.id), (err, bot) => {
    if (err) {
      res.status(404).send("Error 404");
    }
    else {
      res.render('bot', {
        layout: 'layout',
        title: 'Bot ' + req.params.id,
        bot_id: req.params.id,
        owner: bot.owner,
        price: bot.price,
        quan: bot.users,
        image: bot.avatarURL
      });
    }
  });
});

router.get('/:id(\\d+)/image', (req, res) => {
  bots.getById(parseInt(req.params.id), (err, bot) => {
    if (err) {
      res.status(404).send("Error 404");
    }
    else {
      res.sendfile(bot.avatarURL, (err, data) => {
        if (err) {
          res.status(404).send("SendFile error");
        }
      });
    }
  });
});


router.post('/:id(\\d+)', (req, res) => {
  console.log(req.params.id);
  bots.deleteById(req.params.id, (err, data) => {
    if (err) {
      res.send(err.toString());
      res.status(404).send("Error 404");
    }
    else {
      res.redirect('/bots');
    }
  }
  );
});

router.get('/new', (req, res) => {
  res.render('new', {
    title: "New Bot",
    layout: 'layout'
  });
});


router.post('/new', upload.single('image'), (req, res, next) => {
  console.log((req.body.owner));
  let targetPath = '/home/iluha/projects/webprogbase/labs/lab4/data/fs/no_file_available.png';
  if (req.file) {
    //temporary file storage
    const tempPath = req.file.path;
    // main file storage
    targetPath = '/home/iluha/projects/webprogbase/labs/lab4/data/fs/' + req.file.originalname;
    fs.rename(tempPath, targetPath, error => {
      console.log(error);
    });
  }

  bots.insert(req.body.owner, req.body.price, targetPath, req.body.quan, (err, data) => {
    if (err) {
      res.status(404).send("Error 404");
    }
    else {
      // console.log('/bots/' + data.id.toString());
      res.redirect('/bots/' + data.id.toString());

    };
  }
  );
});
module.exports = router;