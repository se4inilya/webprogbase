const users = require('../data/users.json');

class User {

    constructor(id, login, fullname) {
        this.id = id; // number
        this.login = login;  // string
        this.fullname = fullname;  // string
    }

    // static functions to access storage

    // returns user with id or undefined
    static getById(id, callback) {
        
        for(let i = 0; i < users.items.length; i++){
            if (users.items[i].id === id) {
                callback(null,users.items[i]);
            }
        }
        
    }

    // returns an array of all users in storage
    static getAll(callback) {
        callback(null, users.items);
    }
};

module.exports = User;