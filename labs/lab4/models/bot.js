const fs = require('fs');
const bots = require('../data/bots.json');

function parseFile(filepath, callback) {
    fs.readFile(filepath, (err, res) => {
        if (err) {
            callback(err, null);
        }
        else {
            const notes_obj = JSON.parse(res);

            //transforming array of objects into array of Note class objects to use their methods later
            let notes_arr = [];
            for (let i = 0; i < notes_obj.items.length; i++) {
                const note = new Bot(notes_obj.items[i]);
                notes_arr.push(note);
            }
            notes_obj.items = notes_arr;

            callback(null, notes_obj);
        }
    });
}

class Bot {
    constructor(id, owner, price, avatarURL, users) {
        this.id = id;
        this.owner = owner;
        this.price = price;
        this.avatarURL = avatarURL;
        this.users = users;
    }

    static getById(id, callback) {
        for (let i = 0; i < bots.items.length; i++) {
            if (bots.items[i].id === id) {
                callback(null, bots.items[i]);
            }

        }
    }

    static getAll(callback) {
        callback(null, bots.items);
    }


    static deleteById(id, callback) {
        for (let i = 0; i < bots.items.length; i++) {
            console.log(bots.items[i].id);
            if (bots.items[i].id == id) {
                bots.items.splice(i, 1);
            }
        }

        fs.writeFile('./data/bots.json', JSON.stringify(bots, null, 4), (err, data) => {
            if (err)
                callback(err, null);
            else
                callback(null, bots.items);
        });


    }

    static insert(owner, price, avatarURL, users, callback) {
        const x = new Bot(bots.nextId, owner, price, avatarURL, users);
        x.registeredAt = new Date();
        x.registeredAt.toISOString();

        bots.items.push(x);
        bots.nextId += 1;
        fs.writeFile('./data/bots.json', JSON.stringify(bots, null, 4), (err, data) => {
            if (err)
                callback(err, null);

            else
                callback(null, x);

        });

    }

    static update(id, owner, price, avatarURL, users, callback) {
        bots.items.forEach(bot => {
            if (bot.id === id) {
                bot.owner = owner;
                bot.price = price;
                bot.avatarURL = avatarURL;
                bot.users = users;
                fs.writeFile('./data/bots.json', JSON.stringify(bots, null, 4), (err, data) => {
                    if (err)
                    callback(err, null);
                    else
                    callback(null, bots.items);
                });

            }

        });
        
    }
};

module.exports = Bot;

