'use strict';
module.exports = (sequelize, DataTypes) => {
  const categories = sequelize.define('categories', {
    name: DataTypes.STRING
  }, {});
  categories.associate = function(models) {
    categories.belongsToMany(models.bots, {
      through: models.bots_categories,
      as: 'bots',
      foreignKey: 'category_id'
    });
  };
  return categories;
};