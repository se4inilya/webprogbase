const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.render('developer', {
    title: "API v1 reference",
    layout: 'layout',
    currentUser: req.user
  });
});

module.exports = router;