const express = require('express');
const users = require('../models').users;
const router = express.Router();
const protectedRouter = express.Router();
let BotsController = require('../controllers/bots');
let CategoriesController = require('../controllers/categories');
let UsersController = require('../controllers/users');
const authHelpers = require('../auth/_helpers');

//Bots
router.post('/bots/new', BotsController.create);
router.get('/bots/:id', BotsController.getById);
router.get('/bots', BotsController.getAll);
router.delete('/bots/:id/delete', BotsController.deleteById);
router.put('/bots/:id/update', BotsController.updateById);

//Categories
router.post('/categories/new', CategoriesController.create);
router.get('/categories/:id', CategoriesController.getById);
router.get('/categories', CategoriesController.getAll);
router.delete('/categories/:id/delete', CategoriesController.deleteById);
router.put('/categories/:id/update', CategoriesController.updateById);

//Users
protectedRouter.post('/users/new', authHelpers.adminRequired, UsersController.create);
protectedRouter.get('/users', authHelpers.adminRequired, UsersController.getAll);
protectedRouter.get('/users/:id', authHelpers.adminRequired, UsersController.getById);
protectedRouter.get('/users/:userId/giveadmin', authHelpers.adminRequired, UsersController.setAdmin);
protectedRouter.get('/users/:userId/revokeadmin', authHelpers.adminRequired, UsersController.revokeAdmin);
protectedRouter.put('/users/:userId/update', UsersController.updateById);
protectedRouter.delete('/users/:id/delete', UsersController.deleteById);

router.get('/me', authHelpers.loginRequired, async (req, res) => {
    let user = await users.findByPk(req.user.id);
    return res.status(200).send(user);
});

module.exports = {
    protected: protectedRouter,
    unprotected: router
}
