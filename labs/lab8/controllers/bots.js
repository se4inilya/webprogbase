const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = {

    create(req, res) {
        let category_id = req.body.category_id;
        return Bots
            .create({
                name: req.body.name,
                userId: 2,
                price: req.body.price,
                avatarURL: req.body.avatarURL,
            })
            .then(bot => {
                BotsCategories.create({
                    category_id: category_id,
                    bot_id: bot.id,
                })
                    .then(() => {
                        Bots
                            .findByPk(bot.id, {
                                include: [{
                                    model: Categories,
                                    as: 'categories',
                                    required: 'false',
                                    attributes: ['id', 'name'],
                                    through: { attributes: [] }
                                }]
                            })
                            .then(bot => { res.status(201).send(bot) })
                            .catch(error => res.status(400).send(error));
                    })
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => {
                res.status(400).send(error);
            })
    },
    async getAll(req, res) {
        if (req.query.search) {
            try {
                let bots = await Bots.findAll({
                    where: {
                        name:
                        {
                            [Op.iLike]: `%${req.query.search}%`
                        }
                    }
                });
                if (bots.length === 0) {
                    Err = "No bots found";
                }
                res.status(200).send(bots);
            }
            catch (error) {
                res.status(400).send(error)
            }


        }
        else {
            const page = parseInt(req.query.page) || 1;
            const pageSize = 3;
            const offset = page * pageSize - pageSize;
            const limit = pageSize;
            try {
                let bots = await Bots
                    .findAndCountAll({
                        offset,
                        limit,
                    })


                res.status(200).send(bots);

            }
            catch (error) { res.status(400).send(error) };
        }
    },
    getById(req, res) {
        return Bots
            .findByPk(req.params.id, {
                include: [{
                    model: Categories,
                    as: 'categories',
                    required: false,
                    attributes: ['id', 'name'],
                    through: { attributes: [] }
                }]
            })
            .then(
                bot => {
                    if (!bot)
                        res.status(404).send({message : 'Not Found'});
                    else
                        res.status(200).send(bot);
                }
            )
            .catch(error => { res.status(400).send(error) });
    },
    deleteById(req, res) {
        return Bots
            .findOne({
                where: {
                    id: req.params.id,
                }
            })
            .then(bot => {
                if (!bot) {
                    return res.status(404).send({
                        message: 'Bot Not Found',
                    });
                }
                return bot
                    .destroy()
                    .then(() => {

                        res.status(204).send();
                    })
                    .catch(error => res.status(400).send(error));
            }
            )
            .catch(error => res.status(400).send(error));
    },

    // update

    async updateById(req, res) {
        try {
            let bot = await Bots
                .findByPk(req.params.id, {
                    include: [{
                        model: Categories,
                        as: 'categories',
                    }]
                });
            if (!bot) {
                return res.status(404).send({
                    message: 'Bot Not Found'
                });
            }
            let updated_bot = await bot.update(req.body, { fields: Object.keys(req.body) });
            return res.status(200).send(updated_bot);
        } catch (error) {
            return res.status(400).send({ message: 'Updating user failed', error: error });
        }
    },
}