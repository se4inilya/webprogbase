const Users = require('../models').users;


module.exports = {
    async create(req, res) {
        try {
            let user = await Users.create({
                username: req.body.username,
                password_hash: req.body.password_hash,
                role: req.body.role,
                email: req.body.email,
                avaUrl: req.body.avaUrl

            })

            return res.status(201).send(user); // TODO: Change return to res.render

        }
        catch (error) {
            return res.status(400).send(error);
        }
    },
    async getAll(req, res) {
        try {
            let categories = await Users.findAll({
                include: [{
                    model: Bots,
                    as: 'bots',
                    required: false,
                    attributes: ['id', 'name'],
                    through: { attributes: [] }
                }]
            })


            return res.status(200).send(categories);

        }
        catch (error) {
            return res.status(400).send(error);
        }
    },
    async getById(req, res) {
        try {
            let user = await Users.findByPk(req.params.id);

            return res.status(200).send(user);
        }

        catch (error) {
            return res.status(400).send(error);
        }
    },
    async setAdmin(req, res) {
        try {
            let user = await Users
                .findByPk(req.params.userId
                );
            if (!user) {
                return res.status(404).send({
                    message: 'User Not Found'
                });
            }
            await user.update({
                role: 1
            });
            return res.status(200).redirect(`/users/${req.params.userId}`);
        } catch (error) {
            return res.status(400).send(error);
        }

    },
    async revokeAdmin(req, res) {
        try {
            let user = await Users
                .findByPk(req.params.userId
                );
            if (!user) {
                return res.status(404).send({
                    message: 'User Not Found'
                });
            }
            await user.update({
                role: 0
            });
            return res.status(200).redirect(`/users/${req.params.userId}`);
        } catch (error) {
            return res.status(400).send(error);
        }

    },
}