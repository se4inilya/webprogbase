'use strict';
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    username: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false
    },
    password_hash: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    role: {
      defaultValue: 0,
      type: DataTypes.INTEGER
    },
    email: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false
    },
    avaUrl: {
      type: DataTypes,
      allowNull: false
    }
  }, {
    hooks: {
      beforeCreate(user, options) {
        const salt = bcrypt.genSaltSync();
        user.password_hash = bcrypt.hashSync(user.password_hash, salt);
      }
    }
  });
  users.associate = function (models) {
    users.hasMany(models.bots, {
      foreignKey: 'userId',
      as: 'bots'
    });
  };

  users.prototype.check_password = function (password, error) {
    if (error) {
      throw error;
    }
    return bcrypt.compareSync(password, this.password_hash);
  };

  return users;
};