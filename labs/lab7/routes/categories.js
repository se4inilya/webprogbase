const express = require('express');
const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;
const router = express.Router();
const fs = require("fs");
const bodyParser = require('body-parser');
const authHelpers = require('../auth/_helpers');


router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));


router.get('/', authHelpers.loginRequired, async (req, res) => {
  try {
    let categories = await Categories.findAll({
      include: [{
        model: Bots,
        as: 'bots',
        required: false,
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }]
    })

    res.render('categories', {
      title: "Categories",
      layout: 'layout',
      categories: categories,
      currentUser: req.user,
    }
    );

  }

  catch (error) { return res.status(400).send(error) };
});



router.get('/:id(\\d+)', authHelpers.loginRequired, async (req, res) => {
  try {
    let category = await Categories.findByPk(req.params.id, {
      include: [{
        model: Bots,
        as: 'bots',
        required: 'false',
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }]
    })

    res.render('category', {
      layout: 'layout',
      title: category.name,
      id: category.id,
      name: category.name,
      currentUser: req.user
    });
  }
  catch (error) { return res.status(400).send(error) };

});


router.post('/:id(\\d+)', authHelpers.loginRequired, async (req, res) => {
  try {
    let category = await Categories.findOne({
      where: {
        id: req.params.id,
      }
    })

    if (!category) {
      return res.status(404).send({
        message: 'Category Not Found',
      });
    }
    category.destroy()
    res.status(204).redirect('/categories')

  }
  catch (error) { return res.status(400).send(error) };
});

router.get('/new', authHelpers.loginRequired, (req, res) => {
  res.render('new_category', {
    title: "New Category",
    layout: 'layout',
    currentUser: req.user
  });
});


router.post('/new', authHelpers.loginRequired, async (req, res, next) => {
  try {
    let category = await Categories.create({
      name: req.body.name
    })

    await BotsCategories.create({
      category_id: category.id,
      bot_id: 5,
    })

    let createdCategory = await Categories.findByPk(category.id, {
      include: [{
        model: Bots,
        as: 'bots',
        required: 'false',
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }]
    })
    res.redirect(`/categories/${createdCategory.id}`)
  }

  catch (error) { return res.status(400).send(error) };

});

router.get('/:id/update', authHelpers.loginRequired, (req, res) => {
  res.render('category_update', {
    title: "Update of Category",
    layout: 'layout',
    id: req.params.id
  });
});

router.post('/:id(\\d+)/update', authHelpers.loginRequired, async (req, res) => {
  try {
    let category = await Categories.findOne({
      where: {
        id: req.params.id,
      }
    });

    if (!category) {
      return res.status(404).send({
        message: 'Category Not Found',
      });
    }

    let updated_category = await category.update({
      name: req.body.name,
    });
    res.status(200).redirect(`/categories/${updated_category.id}`);


  }
  catch (error) { return res.status(400).send(error) };
});

module.exports = router;