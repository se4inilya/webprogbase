const express = require('express');
const users = require('../models/users');
const router = express.Router();
let BotsController = require('../controllers/bots');
let CategoriesController = require('../controllers/categories');
let UsersController = require('../controllers/users');
const authHelpers = require('../auth/_helpers');



router.get('/users', authHelpers.loginRequired, authHelpers.adminRequired, (req, res) => {
    res.end(JSON.stringify(users.getAll(), null, 4), { layout: 'layout' });
});

router.get('/users/:id', (req, res) => {
    const user = users.getById(parseInt(req.params.id));
    if (user === undefined) {
        res.status(404).send("Error 404");
    }
    else {
        res.end(JSON.stringify(user, null, 4), { layout: 'layout' });
    }
});

router.post('/users/new', UsersController.create);

//Bots
router.post('/bots/new', BotsController.create);
router.get('/bots/:id', BotsController.getById);
router.get('/bots',BotsController.getAll);
router.get('/bots/delete', BotsController.deleteById);

//Categories
router.post('/categories/new',  CategoriesController.create);
router.get('/categories/:id', CategoriesController.getById);
router.get('/categories', CategoriesController.getAll);
router.post('/categories/:id/delete', CategoriesController.deleteById);

router.get('/users/:userId/giveadmin', authHelpers.loginRequired, authHelpers.adminRequired, UsersController.setAdmin);
router.get('/users/:userId/revokeadmin', authHelpers.loginRequired, authHelpers.adminRequired, UsersController.revokeAdmin);

module.exports = router;
