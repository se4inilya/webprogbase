const authHelpers = require('../auth/_helpers');

module.exports = function (app, passport) {

    app.post('/auth/register', passport.authenticate('local-signup', {
            successRedirect: '/auth/login',
            failureRedirect: '/auth/register',
            failureFlash: true,
            
        }
    ));

    app.get('/auth/logout', authHelpers.loginRequired, authHelpers.logout);

    app.post('/auth/login', passport.authenticate('local-signin', {
            successRedirect: '/auth/profile',
            failureRedirect: '/auth/login',
            failureFlash: true,
        }
    ));

    app.get('/auth/register', (req, res, next) => {
        res.render('register', {layout: 'layout', title: '', message: req.flash('signupMessage')});
    });
    app.get('/auth/login', (req, res, next) => {
        res.render('login', {layout: 'layout', title: '', message: req.flash('loginMessage')});
    });
    app.get('/auth/profile', authHelpers.loginRequired, (req, res) => {
        res.render('profile', {layout: 'layout', title: '', user: req.user, image: req.user.avaUrl, currentUser: req.user})
    });
};