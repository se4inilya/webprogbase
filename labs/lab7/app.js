const express = require('express');
const path = require('path');
const hbs = require('express-handlebars');
const UsersRoute = require('./routes/users');
const bots = require('./routes/bots');
const categories = require('./routes/categories');
const auth = require('./routes/auth');
const index = require('./routes/index');
let logger = require('morgan');
const about = require('./routes/about');
const api = require('./routes/api');
const bodyParser = require('body-parser');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const flash    = require('connect-flash');
const User = require('./models').users;



const app = express();

app.set('view engine', 'hbs');
app.use('views', express.static(path.join(__dirname, 'views')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.engine('hbs', hbs({
    extname: 'hbs',
    defaultView: '',
    layoutsDir: __dirname + '/views',
    partialsDir: __dirname + '/views/partials',
    helpers: require('./helpers/handlebars'),
}));
app.use(express.static('public'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());
app.use(session({
    secret: "Some_secret^string",
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


app.use('/users', UsersRoute);
app.use('/', index);
app.use('/bots', bots);
app.use('/about', about);
app.use('/api', api);
app.use('/categories', categories);
let authRoute = require('./routes/auth')(app, passport);
require('./auth/passport')(passport, User);


app.use(function (err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error', { layout: 'layout' });
});

app.listen(3012, function () { console.log('Server is ready'); });




