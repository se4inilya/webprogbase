function deleteBot(id) {
    let http = new XMLHttpRequest();
    let url = `/api/v1/bots/${id}`;
    let params = '';
    http.open('DELETE', url, true);

    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    http.onreadystatechange = function () {//Call a function when the state changes.
      if (http.readyState === 4 && http.status === 204) {
        window.location.replace('/bots?page=1');
      }
      if (http.readyState === 4 && http.status === 400) {
        window.location.href = '/auth/login';
    }
    };
    http.send(params);
  }