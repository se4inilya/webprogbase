let http = new XMLHttpRequest();
let url = `/api/v1/bots?page=1`; 
http.onreadystatechange = function () {
    if (http.readyState === 4 && http.status === 200) {
        let res = JSON.parse(http.responseText);
        let pageCount = Math.ceil(res.count / 3);
        if (pageCount == 0) {
            pageCount = 1;
          }
        let bots_template = Handlebars.templates['bots.hbs'];
        
        document.getElementById('Bots').innerHTML = bots_template({bots: res, page: 1, pageCount: pageCount});
        document.getElementById('NextPage').disabled = 1 === pageCount;
        document.getElementById('PrevPage').disabled = true;
    }
    if (http.readyState === 4 && http.status === 400) {
        window.location.href = '/auth/login';
    }
   
};
http.open('GET', url, true);
http.send();

function turnPage(direction) {
    let http = new XMLHttpRequest();
    let page = findGetParameter('page');
    if (direction === 'next') {
        page++;
    } else {
        page--;
    }
    let url = `/api/v1/bots?page=${page}`;


    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {

            let res = JSON.parse(http.responseText);
            console.log('Bots: ', res);
            let pageCount = Math.ceil(res.count / 3);
            if (pageCount == 0) {
                pageCount = 1;
              }
            let bots_template = Handlebars.templates['bots.hbs'];
            console.log('Template: ', bots_template);
            document.getElementById('Bots').innerHTML = bots_template({bots: res, page: page, pageCount: pageCount});
            window.history.pushState(page.toString(), 'Bots', `/bots?page=${page}`);
            document.getElementById('NextPage').disabled = page === pageCount;
            document.getElementById('PrevPage').disabled = page === 1;

        }
        if (http.readyState === 4 && http.status === 400) {
            window.location.href = '/auth/login';
        }
    };
    http.open('GET', url, true);
    http.send()

}

function findGetParameter(parameterName) {
    let result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}