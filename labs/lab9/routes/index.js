const express = require('express');
const router = express.Router();
const authHelpers = require('../auth/_helpers');

router.get('/explore', authHelpers.loginRequired, async (req, res) => {
  const search_str = req.query.search;
  let search_res;
  if (search_str) {
      try {
          search_res = search;
          res.render('explore', {
              layout: 'layout', title: 'Explore', search_str: search_str,
              search_res: search_res,
              currentUser: req.user,
          });

      } catch (error) {
          res.status(404).render('errors/404', {layout: 'layout', title: 'Error', error: error});
      }
  } else {
      res.render('explore', {layout: 'layout', title: 'Explore', currentUser: req.user});
  }
});

router.get('/', (req, res) => {
  res.render('index', {
    title: "Welcome to Telegram-bot store!",
    layout: 'layout',
    currentUser: req.user
  });

  
});

module.exports = router;