const authHelpers = require('../auth/_helpers');
const jwt = require('jsonwebtoken');

module.exports = function (app, passport) {

    app.post('/auth/register', passport.authenticate('local-signup', {
            successRedirect: '/auth/login',
            failureRedirect: '/auth/register',
            failureFlash: true,
            
        }
    ));

    app.get('/auth/logout', authHelpers.loginRequired, authHelpers.logout);

    app.post('/auth/login', passport.authenticate('local-signin', {
        failureRedirect: '/auth/login',
        failureFlash: true,
    }
), (req, res) => {
    const payload = {
        user: req.user
    };
    const token = jwt.sign(payload, 'secret');
    res.setHeader('Cache-Control', 'private');
    res.cookie('jwt', token, {
        httpOnly: true, maxAge: 900000,
        sameSite: true,
        signed: true,
    });
    return res.status(200).redirect(`/users/${req.user.id}`);
});

    app.get('/auth/register', (req, res, next) => {
        res.render('register', {layout: 'layout', title: '', message: req.flash('signupMessage')});
    });
    app.get('/auth/login', (req, res, next) => {
        res.render('login', {layout: 'layout', title: 'Login', message: req.flash('loginMessage')});
    });
    app.get('/auth/profile', authHelpers.loginRequired, (req, res) => {
        console.log(req.user);
        res.render('profile', {layout: 'layout', title: '', user: req.user, image: req.user.avaUrl, currentUser: req.user})
    });
};