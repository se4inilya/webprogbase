const express = require('express');
const router = express.Router();
const fs = require("fs");
let multer = require("multer");
const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
let parser = require('../data/config');
const authHelpers = require('../auth/_helpers');




// router.get('/search', authHelpers.loginRequired, async (req, res) => {
//   let bots = await Bots.findAll({
//     where: {
//       name: {
//         [Op.iLike]: `%${req.query.search}%`
//       }
//     }
//   });
//   let Err = "";
//   if (bots.length == 0) {
//     Err = "No bots found";
//   }
//   res.render('search', {
//     title: "Found Bots",
//     layout: 'layout',
//     bots: bots,
//     ErrorMessage: Err,
//     currentUser: req.user
//   })

// });

router.get('/', authHelpers.loginRequired, async (req, res) => {
  const pageSize = 3;
  let page = parseInt(req.query.page) || 1;
  const offset = page * pageSize - pageSize;
  const limit = pageSize;
  let prevN = page - 1;
  let nextN = page + 1;

  try {
    let bots = await Bots.findAndCountAll({
      where : {
        userId : req.user.id
      },
      offset,
      limit,
      include: [{
        model: Categories,
        as: 'categories',
        required: false,
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }]
    });

    let quanOfPage = Math.ceil(bots.count / limit);
    if (quanOfPage == 0) {
      quanOfPage = 1;
    }
    if (!page || page == 1) {
      page = 1;
      prevN = page;

      if (quanOfPage == 1) {
        nextN = page;
      }
    }
    else if (page == quanOfPage) {
      page = quanOfPage;
      nextN = page;
    }
    res.render('bots', {
      title: "Bots",
      layout: 'layout',
      bots: bots,
      page: page,
      quanOfPage: quanOfPage,
      pagePrevNumber: prevN,
      pageNextNumber: nextN,
      currentUser: req.user
    }
    )
  }
  catch (error) { return res.status(400).send(error) };

});



router.get('/:id(\\d+)', authHelpers.loginRequired, async (req, res) => {
  try {
    let bot = await Bots.findByPk(req.params.id, {
      include: [{
        model: Categories,
        as: 'categories',
        required: false,
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }]
    })

    res.render('bot', {
      layout: 'layout',
      title: bot.name,
      bot: bot,
      currentUser: req.user
    });
  }

  catch (error) { return res.status(400).send(error) };
});


router.get('/:id/update', authHelpers.loginRequired, async (req, res) => {
  let bot = await Bots.findByPk(req.params.id, {
    include: [{
      model: Categories,
      as: 'categories',
      required: false,
      attributes: ['id', 'name'],
      through: { attributes: [] }
    }]
  });

  res.render('bot_update', {
    title: "Update of Bot",
    layout: 'layout',
    bot: bot,
    currentUser: req.user
  });


});

router.post('/:id(\\d+)/update', authHelpers.loginRequired, parser.single('image'), async (req, res) => {
  try {
    let bot = await Bots.findOne({
      where: {
        id: req.params.id,
      }
    });

    if (!bot) {
      res.render('error', {
        message: 'Bot Not Found',
        error : error
      });
    }

    let updated_bot = await bot.update({
      name: req.body.name,
      price: req.body.price,
      avatarURL: req.file.url
    })
    return res.status(200).redirect(`/bots/${updated_bot.id}`)

  }
  catch (error) { return res.status(400).send(error) };
});

router.post('/:id(\\d+)', authHelpers.loginRequired, async (req, res) => {
  try {
    let bot = await Bots.findOne({
      where: {
        id: req.params.id,
      }
    });

    if (!bot) {
      return res.render('error', {
        message: 'Bot Not Found',
        error : error
      });
    }

    bot.destroy();
    res.status(204).redirect('/bots');
  }
  catch (error) { 
    console.log(error);
    return res.status(400).send(error) };
});

router.get('/new', authHelpers.loginRequired, (req, res) => {
  res.render('new', {
    title: "New Bot",
    layout: 'layout'
  });
});

router.post('/new', authHelpers.loginRequired, parser.single('image'), async (req, res, next) => {


  let category_id = 1;
  try {
    let bot = await Bots.create({
      name: req.body.name,
      price: req.body.price,
      avatarURL: req.file.url,
      userId: req.user.id
    });

    await Promise.all([bot, BotsCategories.create({
      category_id: category_id,
      bot_id: bot.id,
    })])


    createdBot = await Bots
      .findByPk(bot.id, {
        include: [{
          model: Categories,
          as: 'categories',
          required: 'false',
          attributes: ['id', 'name'],
          through: { attributes: [] }
        }]
      });

    res.redirect(`/bots/${createdBot.id}`)
  }
  catch (error) { return res.status(400).send(error) };
});


module.exports = router;