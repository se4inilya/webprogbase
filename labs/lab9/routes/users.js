const express = require('express');
const Users = require('../models').users;
const router = express.Router();
const authHelpers = require('../auth/_helpers');


router.get('/:id', authHelpers.loginRequired, authHelpers.adminRequired, async (req, res) => {
  try {
    let user = await Users.findByPk(req.params.id);

    res.render('profile', {
      layout: 'layout',
      title: '',
      user: user,
      image: user.avaUrl,
      currentUser: req.user

    })
  }
  catch (error) { return res.status(400).send(error) };

});

router.get('/', authHelpers.loginRequired, authHelpers.adminRequired, async (req, res) => {
  try {
    let users = await Users.findAll();

    res.render('users', {
      layout: 'layout',
      title: 'Users',
      users: users,
      currentUser: req.user
    }
    );
  }
  catch (error) { return res.status(400).send(error) };
});




module.exports = router;