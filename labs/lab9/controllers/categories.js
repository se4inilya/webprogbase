const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;


module.exports = {
    create(req, res) {
        return Categories
            .create({
                name: req.body.name
            })
            .then(category => {
                res.status(201).send(category); // TODO: Change return to res.render
            })
            .catch(error => {
                res.status(400).send(error);
            })
    },
    getAll(req, res) {
        return Categories
            .findAll({
                include: [{
                    model: Bots,
                    as: 'bots',
                    required: false,
                    attributes: ['id', 'name'],
                    through: { attributes: [] }
                }]
            })
            .then(
                categories => {
                    res.status(200).send(categories);
                }
            )
            .catch(error => { res.status(400).send(error) });
    },
    getById(req, res) {
        return Categories
            .findByPk(req.params.id, {
                include: [{
                    model: Bots,
                    as: 'bots',
                    required: 'false',
                    attributes: ['id', 'name'],
                    through: {attributes: []}
                }]
            })
            .then(
                category => {
                    if (!category)
                    res.render('error', {
                        message: 'Not Found',
                        error : error
                      });
                    else
                        res.status(200).send(category);
                }
            )
            .catch(error => { res.status(400).send(error) });
    },
    deleteById(req, res){
        return Categories
              .findOne({
                  where: {
                      id: req.params.id,
                  }
              })
              .then(category => {
                  if (!category) {
                    res.render('error', {
                        message: 'Not Found',
                        error : error
                      });
                  }
                  return category
                      .destroy()
                      .then(() => {
  
                          res.status(204).send("cool")
                      })
                      .catch(error => res.status(400).send(error));
              }
              )
              .catch(error => res.status(400).send(error));I
    },
    async updateById(req, res) {
        try {
            let category = await Categories
                .findByPk(req.params.id, {
                    include: [{
                        model: Bots,
                        as: 'bots',
                    }]
                });
            if (!category) {
                return res.render('error', {
                    message: 'Not Found',
                    error : error
                  });
            }
            let updated_category = await category.update(req.body, {fields: Object.keys(req.body)});
            return res.status(200).send(updated_category);
        } catch (error) {
            return res.status(400).send({message:'Updating user failed', error: error});
        }
    },
    async search(req, res) {
        try {
            console.log('ASJDNFLKDFNKLWFMDLSKFJmwq');
            let search_res = await Categories.sequelize.query(`SELECT * FROM ${Categories.tableName} WHERE _search @@ plainto_tsquery('english', :query);`, {
                model: Categories,
                replacements: {query: req.query.search},
            });
            return res.status(200).send(search_res);
        } catch (error) {
            return res.status(400).send({message:'Searching for posts failed', error: error});
        }
    }
}