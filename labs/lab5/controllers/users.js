const Users = require('../models').users;


module.exports = {
    create(req, res) {
        return Users
            .create({
                username: req.body.username,
                password_hash: req.body.password_hash,
                role: req.body.role,
                email: req.body.email,
                avaUrl: req.body.avaUrl
                
            })
            .then(user => {
                res.status(201).send(user); // TODO: Change return to res.render
            })
            .catch(error => {
                res.status(400).send(error);
            })
    },
    getAll(req, res) {
        return Users
            .findAll({
                include: [{
                    model: Bots,
                    as: 'bots',
                    required: false,
                    attributes: ['id', 'name'],
                    through: { attributes: [] }
                }]
            })
            .then(
                categories => {
                    res.status(200).send(categories);
                }
            )
            .catch(error => { res.status(400).send(error) });
    },
    getById(req, res) {
        return Users
            .findByPk(req.params.id)
            .then(
                user => {
                    res.status(200).send(user);
                }
            )
            .catch(error => { res.status(400).send(error) });
    }
}