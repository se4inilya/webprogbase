const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;


module.exports = {

    create(req, res) {
        let category_id = req.body.category_id;
        return Bots
            .create({
                name: req.body.name,
                userId: 1,
                price: req.body.price,
                avatarURL: 'testAva',
            })
            .then(bot => {
                BotsCategories.create({
                    category_id: category_id,
                    bot_id: bot.id,
                })
                    .then(() => {
                        Bots
                            .findByPk(bot.id, {
                                include: [{
                                    model: Categories,
                                    as: 'categories',
                                    required: 'false',
                                    attributes: ['id', 'name'],
                                    through: { attributes: [] }
                                }]
                            })
                            .then(bot => { res.status(201).send(bot) })
                            .catch(error => res.status(400).send(error));
                    })
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => {
                res.status(400).send(error);
            })
    },
    getAll(req, res) {
        const page = parseInt(req.query.page) || 1;
        const pageSize = 3;
        const offset = page * pageSize - pageSize;
        const limit = pageSize;
        return Bots
            .findAndCountAll({
                offset,
                limit,
                include: [{
                    model: Categories,
                    as: 'categories',
                    required: false,
                    attributes: ['id', 'name'],
                    through: { attributes: [] }
                }]
            })
            .then(
                bots => {
                    res.status(200).send(bots);
                }
            )
            .catch(error => { res.status(400).send(error) });
    },
    getById(req, res) {
        return Bots
            .findByPk(req.params.id, {
                include: [{
                    model: Categories,
                    as: 'categories',
                    required: false,
                    attributes: ['id', 'name'],
                    through: { attributes: [] }
                }]
            })
            .then(
                bot => {
                    res.status(200).send(bot);
                }
            )
            .catch(error => { res.status(400).send(error) });
    },
    deleteById(req, res) {
        return Bots
            .findOne({
                where: {
                    id: req.params.id,
                }
            })
            .then(bot => {
                if (!bot) {
                    return res.status(404).send({
                        message: 'Bot Not Found',
                    });
                }
                if (bot.avatarURL !== '/home/iluha/projects/webprogbase/labs/lab5/data/fs/no_file_available.png') {
                    fs.unlink(bot.avatarURL, (err) => {
                        if (err) {
                            res.status(400).send(err);
                        }
                    }
                    );
                }
                return bot
                    .destroy()
                    .then(() => {

                        res.status(204).redirect('http://localhost:3001/bots')
                    })
                    .catch(error => res.status(400).send());
            }
            )
            .catch(error => res.status(400).send(error));
    },

    // update

    updateById(req, res) {
        return Posts
            .findOne({
                where: {
                    id: req.params.postId,
                    userId: req.params.userId
                }
            })
            .then(post => {
                if (!post) {
                    return res.status(404).send({
                        message: 'Post Not Found',
                    });
                }
                let targetPath = post.imageUri;
                if (req.file) {
                    if (post.imageUri !== '/home/dizzzmas/PycharmProjects/webprogbase/labs/lab5/data/fs/no_file_available.png') {
                        fs.unlink(post.imageUri, (err) => {
                            if (err) {
                                res.status(400).send(err);
                            }
                        }
                        );
                    }
                    const tempPath = req.file.path;
                    targetPath = '/home/dizzzmas/PycharmProjects/webprogbase/labs/lab5/data/fs/' + req.file.originalname;
                    fs.rename(tempPath, targetPath, error => {
                        console.log(error);
                    });


                }
                return post
                    .update({
                        content: req.body.content || post.content,
                        imageUri: targetPath || post.imageUri
                    })
                    .then(updated_post => res.status(200).redirect(`http://localhost:3001/posts/${updated_post.id}`))
                    .catch(error => res.status(400).send(error));
            })
            .catch(error => res.status(400).send(error));
    }
}