const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;


module.exports = {
    create(req, res) {
        return Categories
            .create({
                name: req.body.name
            })
            .then(category => {
                res.status(201).send(category); // TODO: Change return to res.render
            })
            .catch(error => {
                res.status(400).send(error);
            })
    },
    getAll(req, res) {
        return Categories
            .findAll({
                include: [{
                    model: Bots,
                    as: 'bots',
                    required: false,
                    attributes: ['id', 'name'],
                    through: { attributes: [] }
                }]
            })
            .then(
                categories => {
                    res.status(200).send(categories);
                }
            )
            .catch(error => { res.status(400).send(error) });
    },
    getById(req, res) {
        return Categories
            .findByPk(req.params.id, {
                include: [{
                    model: Bots,
                    as: 'bots',
                    required: 'false',
                    attributes: ['id', 'name'],
                    through: {attributes: []}
                }]
            })
            .then(
                category => {
                    res.status(200).send(category);
                }
            )
            .catch(error => { res.status(400).send(error) });
    },
    deleteById(req, res){
        return Categories
              .findOne({
                  where: {
                      id: req.params.id,
                  }
              })
              .then(category => {
                  if (!category) {
                      return res.status(404).send({
                          message: 'Category Not Found',
                      });
                  }
                  return category
                      .destroy()
                      .then(() => {
  
                          res.status(204).send("cool")
                      })
                      .catch(error => res.status(400).send(error));
              }
              )
              .catch(error => res.status(400).send(error));I
    }
}