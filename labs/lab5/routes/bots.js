const express = require('express');
const router = express.Router();
const fs = require("fs");
let multer = require("multer");
const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;


const upload = multer({
  dest: '/home/iluha/projects/webprogbase/labs/lab4/data/temp'
});


router.get('/search', (req, res) => {
  Bots.findAll({
    where: {
      name: {
        [Op.iLike]: `%${req.query.search}%`
      }
    }
  }).then(bots => {
    let Err = "";
    if(bots.length === 0){
      Err = "No bots found";
    }
    res.render('search', {
      title: "Found Bots",
      layout: 'layout',
      bots: bots,
      ErrorMessage : Err
    })
  })});

  router.get('/', (req, res) => {
    const pageSize = 3;
    let page = parseInt(req.query.page) || 1;
    const offset = page * pageSize - pageSize;
    const limit = pageSize;
    let prevN = page - 1;
    let nextN = page + 1;

    return Bots
      .findAndCountAll({
        offset,
        limit,
        include: [{
          model: Categories,
          as: 'categories',
          required: false,
          attributes: ['id', 'name'],
          through: { attributes: [] }
        }]
      })
      .then(bots => {
        let quanOfPage = Math.ceil(bots.count / limit);
        if (quanOfPage == 0) {
          quanOfPage = 1;
        }
        if (!page || page == 1) {
          page = 1;
          prevN = page;

          if (quanOfPage == 1) {
            nextN = page;
          }
        }
        else if (page == quanOfPage) {
          page = quanOfPage;
          nextN = page;
        }
        res.render('bots', {
          title: "Bots",
          layout: 'layout',
          bots: bots,
          page: page,
          quanOfPage: quanOfPage,
          pagePrevNumber: prevN,
          pageNextNumber: nextN,
        }
        )
      })
      .catch(error => { res.status(400).send(error) });

  });



  router.get('/:id(\\d+)', (req, res) => {
    return Bots
      .findByPk(req.params.id, {
        include: [{
          model: Categories,
          as: 'categories',
          required: false,
          attributes: ['id', 'name'],
          through: { attributes: [] }
        }]
      })
      .then(bot => {
        res.render('bot', {
          layout: 'layout',
          title: bot.name,
          bot: bot,
        });
      })

      .catch(error => { res.status(400).send(error) });
  });

  router.get('/:id(\\d+)/image', (req, res) => {
    return Bots
      .findByPk(req.params.id, {
        include: [{
          model: Categories,
          as: 'categories',
          required: false,
          attributes: ['id', 'name'],
          through: { attributes: [] }
        }]
      })
      .then(bot => {
        res.sendfile(bot.avatarURL);
      })

      .catch(error => { res.status(400).send(error) });
  });

  router.get('/:id/update', (req, res) => {
    res.render('bot_update', {
      title: "Update of Bot",
      layout: 'layout',
      id: req.params.id
    });
  });

  router.post('/:id(\\d+)/update', upload.single('image'), (req, res) => {
    return Bots
      .findOne({
        where: {
          id: req.params.id,
        }
      })
      .then(bot => {
        if (!bot) {
          return res.status(404).send({
            message: 'Bot Not Found',
          });
        }
        let targetPath = bot.avatarURL;
        if (req.file) {
          if (bot.avatarURL !== '/home/iluha/projects/webprogbase/labs/lab5/data/fs/no_file_available.png') {
            fs.unlink(bot.avatarURL, (err) => {
              if (err) {
                res.status(400).send(err);
              }
            }
            );
          }
          const tempPath = req.file.path;
          targetPath = '/home/iluha/projects/webprogbase/labs/lab5/data/fs/' + req.file.originalname;
          fs.rename(tempPath, targetPath, error => {
            console.log(error);
          });


        }
        return bot
          .update({
            name: req.body.name || bot.name,
            price: req.body.price || bot.price,
            avatarURL: targetPath || bot.avatarURL
          })
          .then(updated_bot => res.status(200).redirect(`/bots/${updated_bot.id}`))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  });

  router.post('/:id(\\d+)', (req, res) => {
    return Bots
      .findOne({
        where: {
          id: req.params.id,
        }
      })
      .then(bot => {
        if (!bot) {
          return res.status(404).send({
            message: 'Bot Not Found',
          });
        }
        if (bot.avatarURL !== '/home/iluha/projects/webprogbase/labs/lab5/data/fs/no_file_available.png') {
          fs.unlink(bot.avatarURL, (err) => {
            if (err) {
              res.status(400).send(err);
            }
          }
          );
        }
        return bot
          .destroy()
      }
      )
      .then(() => {

        res.status(204).redirect('/bots')
      })
      .catch(error => res.status(400).send(error)); I
  });

  router.get('/new', (req, res) => {
    res.render('new', {
      title: "New Bot",
      layout: 'layout'
    });
  });


  router.post('/new', upload.single('image'), (req, res, next) => {
    let targetPath = '/home/iluha/projects/webprogbase/labs/lab4/data/fs/no_file_available.png';
    if (req.file) {
      //temporary file storage
      const tempPath = req.file.path;
      // main file storage
      targetPath = '/home/iluha/projects/webprogbase/labs/lab4/data/fs/' + req.file.originalname;
      fs.rename(tempPath, targetPath, error => {
        console.log(error);
      });
    }

    let category_id = 1;
    return Bots
      .create({
        name: req.body.name,
        price: req.body.price,
        avatarURL: targetPath,
        userId: 1
      })
      .then(bot => {
        return Promise.all([bot, BotsCategories.create({
          category_id: category_id,
          bot_id: bot.id,
        })]);
      })
      .then(([bot, category]) => {
        return Bots
          .findByPk(bot.id, {
            include: [{
              model: Categories,
              as: 'categories',
              required: 'false',
              attributes: ['id', 'name'],
              through: { attributes: [] }
            }]
          });
      })
      .then(bot => { res.redirect(`/bots/${bot.id}`) })
      .catch(error => res.status(400).send(error));
  });


  module.exports = router;