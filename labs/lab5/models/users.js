'use strict';
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    username: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false
    },
    password_hash: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    role: {
      defaultValue: 0,
      type: DataTypes.INTEGER
    },
    email: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false
    },
    avaUrl: {
      type: DataTypes,
      allowNull: false
    }
  }, {});
  users.associate = function (models) {
    users.hasMany(models.bots, {
      foreignKey: 'userId',
      as: 'bots'
    })
  };
  return users;
};