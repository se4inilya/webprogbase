'use strict';
//const Categories = require('./categories');
module.exports = (sequelize, DataTypes) => {
  const bots = sequelize.define('bots', {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    avatarURL: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {});
  bots.associate = function (models) {
    bots.belongsTo(models.users, {
      foreignKey : 'userId',
      onDelete : 'CASCADE'
    })
    bots.belongsToMany(models.categories, {
      through: models.bots_categories,
      as: 'categories',
      foreignKey: 'bot_id',
    })
  };
  return bots;
};