const express = require('express');
const router = express.Router();
const fs = require("fs");
let multer = require("multer");
const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
let parser = require('../data/config');




router.get('/search', (req, res) => {
  Bots.findAll({
    where: {
      name: {
        [Op.iLike]: `%${req.query.search}%`
      }
    }
  }).then(bots => {
    let Err = "";
    if (bots.length === 0) {
      Err = "No bots found";
    }
    res.render('search', {
      title: "Found Bots",
      layout: 'layout',
      bots: bots,
      ErrorMessage: Err
    })
  })
});

router.get('/', (req, res) => {
  const pageSize = 3;
  let page = parseInt(req.query.page) || 1;
  const offset = page * pageSize - pageSize;
  const limit = pageSize;
  let prevN = page - 1;
  let nextN = page + 1;

  return Bots
    .findAndCountAll({
      offset,
      limit,
      include: [{
        model: Categories,
        as: 'categories',
        required: false,
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }]
    })
    .then(bots => {
      let quanOfPage = Math.ceil(bots.count / limit);
      if (quanOfPage == 0) {
        quanOfPage = 1;
      }
      if (!page || page == 1) {
        page = 1;
        prevN = page;

        if (quanOfPage == 1) {
          nextN = page;
        }
      }
      else if (page == quanOfPage) {
        page = quanOfPage;
        nextN = page;
      }
      res.render('bots', {
        title: "Bots",
        layout: 'layout',
        bots: bots,
        page: page,
        quanOfPage: quanOfPage,
        pagePrevNumber: prevN,
        pageNextNumber: nextN,
      }
      )
    })
    .catch(error => { res.status(400).send(error) });

});



router.get('/:id(\\d+)', (req, res) => {
  return Bots
    .findByPk(req.params.id, {
      include: [{
        model: Categories,
        as: 'categories',
        required: false,
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }]
    })
    .then(bot => {
      res.render('bot', {
        layout: 'layout',
        title: bot.name,
        bot: bot,
      });
    })

    .catch(error => { res.status(400).send(error) });
});


router.get('/:id/update', (req, res) => {
  return Bots
    .findByPk(req.params.id, {
      include: [{
        model: Categories,
        as: 'categories',
        required: false,
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }]
    })
    .then(bot => {
      res.render('bot_update', {
        title: "Update of Bot",
        layout: 'layout',
        bot : bot
      });
    })

});

router.post('/:id(\\d+)/update', parser.single('image'), (req, res) => {
  return Bots
    .findOne({
      where: {
        id: req.params.id,
      }
    })
    .then(bot => {
      if (!bot) {
        return res.status(404).send({
          message: 'Bot Not Found',
        });
      }

      return bot
        .update({
          name: req.body.name,
          price: req.body.price,
          avatarURL: req.file.url
        })
        .then(updated_bot => res.status(200).redirect(`/bots/${updated_bot.id}`))
        .catch(error => res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
});

router.post('/:id(\\d+)', (req, res) => {
  return Bots
    .findOne({
      where: {
        id: req.params.id,
      }
    })
    .then(bot => {
      if (!bot) {
        return res.status(404).send({
          message: 'Bot Not Found',
        });
      }

      return bot
        .destroy()
    }
    )
    .then(() => {

      res.status(204).redirect('/bots')
    })
    .catch(error => res.status(400).send(error)); I
});

router.get('/new', (req, res) => {
  res.render('new', {
    title: "New Bot",
    layout: 'layout'
  });
});


router.post('/new', parser.single('image'), (req, res, next) => {


  let category_id = 1;
  return Bots
    .create({
      name: req.body.name,
      price: req.body.price,
      avatarURL: req.file.url,
      userId: 1
    })
    .then(bot => {
      return Promise.all([bot, BotsCategories.create({
        category_id: category_id,
        bot_id: bot.id,
      })]);
    })
    .then(([bot, category]) => {
      return Bots
        .findByPk(bot.id, {
          include: [{
            model: Categories,
            as: 'categories',
            required: 'false',
            attributes: ['id', 'name'],
            through: { attributes: [] }
          }]
        });
    })
    .then(bot => { res.redirect(`/bots/${bot.id}`) })
    .catch(error => res.status(400).send(error));
});


module.exports = router;