const express = require('express');
const Categories = require('../models').categories;
const Bots = require('../models').bots;
const BotsCategories = require('../models').bots_categories;
const router = express.Router();
const fs = require("fs");
const bodyParser = require('body-parser');


router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));


router.get('/', (req, res) => {
    return Categories
        .findAll({
            include: [{
                model: Bots,
                as: 'bots',
                required: false,
                attributes: ['id', 'name'],
                through: { attributes: [] }
            }]
        })
        .then(
            categories => {
                res.render('categories', {
                    title: "Categories",
                    layout: 'layout',
                    categories: categories,
                }
                );

            }
        )
        .catch(error => { res.status(400).send(error) });
});



router.get('/:id(\\d+)', (req, res) => {
    return Categories
            .findByPk(req.params.id, {
                include: [{
                    model: Bots,
                    as: 'bots',
                    required: 'false',
                    attributes: ['id', 'name'],
                    through: {attributes: []}
                }]
            })
            .then(
                category => {
                    res.render('category', {
                                layout: 'layout',
                                title: category.name,
                                id : category.id,
                                name : category.name
                              });
                }
            )
            .catch(error => { res.status(400).send(error) });

});


router.post('/:id(\\d+)', (req, res) => {
    return Categories
              .findOne({
                  where: {
                      id: req.params.id,
                  }
              })
              .then(category => {
                  if (!category) {
                      return res.status(404).send({
                          message: 'Category Not Found',
                      });
                  }
                  return category
                      .destroy()
                      .then(() => {
  
                          res.status(204).redirect('/categories')
                      })
                      .catch(error => res.status(400).send(error));
              }
              )
              .catch(error => res.status(400).send(error));I
  });

router.get('/new', (req, res) => {
    res.render('new_category', {
        title: "New Category",
        layout: 'layout'
    });
});


router.post('/new', (req, res, next) => {
    return Categories
            .create({
                name: req.body.name
            })
            .then(category => {
                BotsCategories.create({
                  category_id: category.id,
                  bot_id: 1,
                })
                  .then(() => {
                    Categories
                      .findByPk(category.id, {
                        include: [{
                          model: Bots,
                          as: 'bots',
                          required: 'false',
                          attributes: ['id', 'name'],
                          through: { attributes: [] }
                        }]
                      })
                      .then(category => {res.redirect(`/categories/${category.id}`)})
                      .catch(error => res.status(400).send(error));
                  })
                  .catch(error => res.status(400).send(error));
              })
              .catch(error => {
                res.status(400).send(error);
              })
});

router.get('/:id/update', (req, res) => {
    res.render('category_update', {
      title: "Update of Category",
      layout: 'layout',
      id : req.params.id
    });
  });
  
  router.post('/:id(\\d+)/update', (req, res) => {
    return Categories
      .findOne({
        where: {
          id: req.params.id,
        }
      })
      .then(category => {
        if (!category) {
          return res.status(404).send({
            message: 'Category Not Found',
          });
        }
        
        return category
          .update({
            name: req.body.name,
          })
          .then(updated_category => res.status(200).redirect(`/categories/${updated_category.id}`))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  });

module.exports = router;