const express = require('express');
const Users = require('../models').users;
const router = express.Router();


router.get('/:id', (req, res) => {
  return Users
    .findByPk(req.params.id)
    .then(user => {
      res.render('user', {
        layout: 'layout',
        title: 'User ' + req.params.id,
        username: user.username,
        email: user.email,
        image: user.avaUrl
      });
    })

    .catch(error => { res.status(400).send(error) });

});

router.get('/', (req, res) => {
  return Users
    .findAll()
    .then(
      users => {
        console.log(users.rows);
        res.render('users', {
          layout: 'layout',
          title: 'Users',
          users: users
        });
      }
    )
    .catch(error => { res.status(400).send(error) });
});




module.exports = router;