const users = require('../data/users.json');

class User {

    constructor(id, login, fullname) {
        this.id = id; // number
        this.login = login;  // string
        this.fullname = fullname;  // string
    }

    // static functions to access storage

    // returns user with id or undefined
    static getById(id) {
        
        for(let i = 0; i < users.items.length; i++){
            if (users.items[i].id === id) {
                return users.items[i];
            }
        }
        return undefined;
    }

    // returns an array of all users in storage
    static getAll() {
        return users.items;
    }
};

module.exports = User;