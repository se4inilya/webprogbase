const user = require('./models/user');
const bot = require('./models/bot');
const readline = require('readline-sync');

console.log('Users:\n');
console.log(user.getAll());
console.log('\nBots :\n');
console.log(bot.getAll());
while (true) {
    const inputString = readline.question("Input commands: ");
    //    

    if (inputString === "users")
        console.log(user.getAll());
    else if (inputString.includes("users/get/")) {
        const num = inputString.substring(inputString.length, 10);
        const gotuser = user.getById(parseInt(num));
        if (gotuser !== undefined)
            console.log(gotuser);
        else
            console.log('User with this id does not exsist.');
    }
    else if (inputString === "bots")
        console.log(bot.getAll());
    else if (inputString === "bots/insert") {
        const owner = readline.question('Write owner\'s name : ');
        const price = readline.questionInt('Write price : ');
        const category = readline.question('Write category : ');
        const users = readline.questionInt('Write quantity of users : ');

        console.log(bot.insert(owner, price, category, users));
    }
    else if (inputString.includes("bots/get/")) {
        const num = inputString.substring(inputString.length, 9);
        const gotbot = bot.getById(parseInt(num));
        if (gotbot !== undefined)
            console.log(gotbot);
        else
            console.log('Bot with this id does not exsist.');
    }
    else if (inputString.includes("bots/delete/")) {
        const num = inputString.substring(inputString.length, 12);
        const gotbot = bot.deleteById(parseInt(num));
        if (gotbot !== undefined)
            console.log('Selected bot was deleted.');
        else
            console.log('Bot with this id does not exsist.');
    }
    else if (inputString.includes("bots/update/")) {
        const num = inputString.substring(inputString.length, 12);
        if(bot.getById(parseInt(num)) === undefined){
            console.log('Bot with this id does not exsist.');
        }
        else{
        const owner = readline.question('Write owner\'s name : ');
        const price = readline.questionInt('Write price : ');
        const category = readline.question('Write category : ');
        const users = readline.questionInt('Write quantity of users : ');

        bot.update(parseInt(num), owner, price, category, users);
        console.log('Selected bot was updated.');
        }
    }
    else
        console.log("Comands : users, users/get/{id}, bots, bots/get/{id}, bots/update/{id}, bots/delete/{id}, bots/insert ");

}

