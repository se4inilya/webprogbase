const express = require('express');
const users = require('../models/user');
const router = express.Router();
const usersJSON = require('../data/users');


router.get('/:id', (req, res) => {
  console.log('ID route called');
  const user = users.getById(parseInt(req.params.id));
  if (user === undefined) {
    res.status(404).send("Error 404");
  }
  else {
    res.render('user', {
      layout: 'layout',
      title: 'User ' + req.params.id,
      login: user.login,
      fullname: user.fullname,
      registeredAt: user.registeredAt,
      image: user.avaUrl
    });
  }
});

router.get('/', (req, res) => {
  res.render('users', {
    layout: 'layout',
    title: 'Users',
    users: usersJSON.items
  });
});


module.exports = router;