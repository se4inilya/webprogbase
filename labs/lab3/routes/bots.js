const express = require('express');
const bots = require("../models/bot");
const router = express.Router();

router.get('/', (req, res) => {
  res.render('bots', {
    title: "Bots",
    layout: 'layout'
  });
});

router.get('/:id', (req, res) => {
  const bot = bots.getById(parseInt(req.params.id));
  if (bot === undefined) {
    res.status(404).send("Error 404");
  }
  else {
    res.render('bot', {
      layout: 'layout',
      title: 'Bot ' + req.params.id,
      bot_id: req.params.id,
      owner: bot.owner,
      price: bot.price,
      quan: bot.users
    }
    );
  }
});


module.exports = router;