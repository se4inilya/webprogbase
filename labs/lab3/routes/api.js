const express = require('express');
const users = require('../models/user');
const router = express.Router();

router.get('/users', (req, res) => {
    res.end(JSON.stringify(users.getAll(), null, 4), { layout: 'layout' });
});

router.get('/users/:id', (req, res) => {
    const user = users.getById(parseInt(req.params.id));
    if (user === undefined) {
        res.status(404).send("Error 404");
    }
    else {
        res.end(JSON.stringify(user, null, 4), { layout: 'layout' });
    }
});

module.exports = router;
