const express = require('express');
const path = require('path');
const hbs = require('express-handlebars');
const UsersRoute = require('./routes/users');
const bots = require('./routes/bots');
const index = require('./routes/index');
let logger = require('morgan');
const about = require('./routes/about');
const api = require('./routes/api');


const app = express();

app.set('view engine', 'hbs');
app.use('views', express.static(path.join(__dirname, 'views')));

app.engine('hbs', hbs({
    extname: 'hbs',
    defaultView: '',
    layoutsDir: __dirname + '/views',
    partialsDir: __dirname + '/views/partials'
}));
app.use(express.static('public'));

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/users', UsersRoute);
app.use('/', index);
app.use('/bots', bots);
app.use('/about', about);
app.use('/api', api);

app.use(function (err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error', { layout: 'layout' });
});

app.listen(3009, function () { console.log('Server is ready'); });




