const fs = require('fs');
const bots = require('../data/bots.json');

class Bot {
    constructor(id, owner, price, category, users) {
        this.id = id;
        this.owner = owner;
        this.price = price;
        this.category = category;
        this.users = users;
    }

    static getById(id) {
        for(let i = 0; i < bots.items.length; i++){
            if (bots.items[i].id === id) {
                return bots.items[i];
            }
            
        }
        return undefined;
    }

    static getAll() {
        return bots.items;
    }

    static deleteById(id) {
        let deletedBot;
        let i = 0;
        bots.items.forEach(bot => {
            if (bot.id === id) {
                deletedBot = bots.items.splice(i, 1);
            }
            i++;
        });
        if (deletedBot !== undefined) {

            fs.writeFileSync('./data/bots.json', JSON.stringify(bots, null, 4));
        }
        return deletedBot;
    }

    static insert(owner, price, category, users) {
        const x = new Bot(bots.nextId, owner, price, category, users);
        x.registeredAt = new Date();
        x.registeredAt.toISOString();
        bots.items.push(x);
        bots.nextId += 1;
        fs.writeFileSync('./data/bots.json', JSON.stringify(bots, null, 4));
        return '/nBot was added.';
    }

    static update(id, owner, price, category, users) {
        bots.items.forEach(bot => {
            if (bot.id === id) {
                bot.owner = owner;
                bot.price = price;
                bot.category = category;
                bot.users = users;
                fs.writeFileSync('./data/bots.json', JSON.stringify(bots, null, 4));
                return '\nBot was updated.\n';
            }

        });

    }
};

module.exports = Bot;

